import asyncio
import click
import os
import sys
from flask import Flask, render_template, url_for, redirect, send_from_directory, request
from collect import runall as run_collect

import logging
logging.basicConfig(level=logging.DEBUG)


from flask_cors import CORS


loop = asyncio.get_event_loop()
app = Flask(__name__, template_folder=os.path.join(os.path.dirname(__file__),  'web/build'), static_folder=os.path.join(os.path.dirname(__file__), 'web/build'), static_url_path='')
analyze_image = lambda path, **kwargs: asyncio.run(run_collect(path, **kwargs))

CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/asdfasd')
def index2():
    url = url_for('static', filename='bundle.js')
    image_uplaod = url_for('static', filename='imageUpload.js')
    return render_template('index.html', bundle=url, imageUpload=image_uplaod)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/analyze', methods=['POST'])
def analyze():
    uploaded_file = request.files['file']
    print(f"saving image {uploaded_file.filename}")
    if uploaded_file.filename != '':
        uploaded_file.save(uploaded_file.filename)
    return ''.join(analyze_image(uploaded_file.filename))

   
@click.command(context_settings={"ignore_unknown_options": True})
@click.argument('paths', nargs=-1)
@click.option('--nats-servers', envvar='NATS_SERVER', multiple=True, default=["127.0.0.1:4222"])
@click.option('--port', default=3134)
@click.option('--host', default='0.0.0.0')
def main(nats_servers: [str] = [], paths: [str] = [], port=None, host = None, **kwargs):
    analyze_image = lambda path: asyncio.run(analyze_image(path, nats_servers=nats_servers, **kwargs))
    app.run(debug=True, port=port, host=host, threaded=True)

if __name__ == "__main__":
    main()
    
