import asyncio
import numpy as np
import os

import threading

from logging import getLogger, getLevelName, Formatter, StreamHandler
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers

from .dto import ImageDTO

log = getLogger(__file__)
log.setLevel(getLevelName("DEBUG"))
log_formatter = Formatter(
    "%(asctime)s [%(levelname)s] %(name)s: %(message)s [%(threadName)s] "
)  # I am printing thread id here
log.propagate = False

console_handler = StreamHandler()
console_handler.setFormatter(log_formatter)
log.addHandler(console_handler)


def add_message_broker_params(parser):
    parser.add_argument("brokers", type=str, nargs="+", help="urls of broker servers")

    return parser


class NATSImageBroker:
    """
    Abstracts logic of sending and recieving messages with images using NATS messaging system

    Supports async with
    """

    def __init__(self, **nats_options):

        if "servers" in nats_options and len(nats_options["servers"]) > 0:
            nats_options["servers"] = [
                s if s.startswith("nats://") else f"nats://{s}"
                for s in nats_options["servers"]
            ]

        # Callback functions for debug logging
        async def error_cb(e):
            log.error(f"NATS Error: {type(e)}")
            # log.error("NATS Error:", e),

        async def closed_cb():
            log.info("NATS Connection to NATS is closed.")

        async def reconnected_cb():
            log.info(f"NATS Connected to NATS ")

        self.nats_options = {
            "loop": asyncio.get_event_loop(),
            "error_cb": error_cb,
            "closed_cb": closed_cb,
            "reconnected_cb": reconnected_cb,
            "max_reconnect_attempts": 5,
            "reconnect_time_wait": 2,
        }

        for opt in nats_options:
            self.nats_options[opt] = nats_options[opt]

        self.loop = self.nats_options["loop"]
        self.nc = NATS()

    async def __aenter__(self):
        cr = await self.nc.connect(**self.nats_options, connect_timeout=10)
        log.info(f"NATS Connected")
        return self

    async def __aexit__(self, type, value, traceback):
        log.info("NATS Exiting")
        if self.nc.is_connected:
            log.info("NATS Closing")
            await self.nc.close()

    async def _message_handler(self, msg):
        log.debug(f"Received a message on '{msg.subject} / {msg.reply}'")
        dto = ImageDTO.from_nats_message(msg)
        if bool(dto):
            log.debug(f"Received an image '{dto.name} with color {dto.color}'")
        return dto

    async def reply_to_image(self, image: ImageDTO):
        if bool(image.nats_reply_subject):
            log.debug(
                f"Replying to image {image.name} with {image.color} ({image.nats_reply_subject})"
            )
            await self.nc.publish(image.nats_reply_subject, image.color.encode())

    async def send_image(
        self, image: ImageDTO, subject: str = "analyze", want_reply: bool = False
    ) -> ImageDTO:
        log.debug(
            f"{'Sending with reply' if want_reply else 'Sending'} image {image.name} with subject {subject}"
        )
        payload = image.to_nats_msg()

        result = None
        if want_reply:
            reply = await self.nc.request(subject, payload, timeout=10)
            log.debug(f"Received reply {reply.data}")
            result = reply.data.decode()
        else:
            await self.nc.publish(subject, payload)

        return result

    async def subscribe(
        self, subject: str = "analyze", max_msgs: int = 0
    ) -> asyncio.Queue:

        queue = asyncio.Queue()

        async def queue_handler(msg):
            data = await self._message_handler(msg)
            await queue.put(data)

        sid = await self.nc.subscribe(
            subject, cb=queue_handler, max_msgs=max_msgs, is_async=True
        )

        log.debug(f"Subscribed on '{subject}")
        return queue, sid