import asyncio
import base64
import cv2
import json
import numpy as np
import pickle
import os
from datetime import datetime
from pydantic import dataclasses
from .analyzer import get_image_color_avg


@dataclasses.dataclass(frozen=False)
class ImageDTO:
    """
    DTO to transmit the image and it's analyzed metadata
    """

    name: str
    data: bytes
    color: str = None
    created: datetime = datetime.now()
    nats_reply_subject: str = None

    @staticmethod
    def from_file(path: str):
        with open(path, "rb") as f:
            return ImageDTO(name=os.path.basename(path), data=f.read())

    @staticmethod
    def from_nats_message(msg):
        result = pickle.loads(msg.data)

        if bool(msg.reply and msg.reply.strip()):
            result.nats_reply_subject = msg.reply

        return result

    def to_nats_msg(self):
        return pickle.dumps(self)

    def to_cv2_object(self):
        nparr = np.frombuffer(self.data, np.uint8)
        return cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    def calculate_average_color(self):
        if self.data is not None:
            self.color = get_image_color_avg(self.to_cv2_object())
            return self.color

    def save(self, path=None):
        if path == None:
            path = self.name

        with open(path, "wb") as f:
            f.write(self.data)
            return path