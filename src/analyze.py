import asyncio
import click
import sys
import os

import numpy as np
from imagelib import ImageDTO, NATSImageBroker

import logging

logging.basicConfig(level=logging.DEBUG)


async def run(max_msgs=0, nats_servers: [str] = [os.environ.get("NATS_SERVER", "127.0.0.1:4222")], **kwargs):

    async with NATSImageBroker(servers=nats_servers, **kwargs) as broker:
        queue, _ = await broker.subscribe(max_msgs=max_msgs)

        while True:
            new_image = await queue.get()
            new_image.calculate_average_color()
            await broker.send_image(new_image, want_reply=False, subject="analyzed")


@click.command(context_settings={"ignore_unknown_options": True})
@click.argument('paths', nargs=-1)
@click.option('--nats-servers', envvar='NATS_SERVER', multiple=True, default=["127.0.0.1:4222"])
def main(nats_servers: [str] = [], **kwargs):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(nats_servers=nats_servers))

if __name__ == "__main__":
    main()
