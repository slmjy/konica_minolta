import asyncio
import click
import logging
import os
import sys

import numpy as np

from imagelib import (ImageDTO, NATSImageBroker, get_closest_webcolor,
                      is_file_image)


def scan_dirs(*paths, follow_symlinks=True, recurse=True):

    failed = {}
    images = []

    def read_image(path):
        try:
            if is_file_image(path):
                images.append(ImageDTO.from_file(path))
        except Exception as error:
            failed[path] = error
            logging.error(f"{path}", error)

    for path in paths:
        if os.path.isfile(path):
            read_image(path)
            continue

        for entry in os.scandir(path):
            if entry.is_file():
                read_image(entry.path)
            elif recurse and entry.is_dir():
                yield from scan_dirs(
                    entry.path, follow_symlinks=follow_symlinks, recurse=True
                )

    yield from images


async def run(*paths, want_reply=True,  nats_servers: [str] = [os.environ.get("NATS_SERVER", "127.0.0.1:4222")], **kwargs):

    async with NATSImageBroker(servers=nats_servers, **kwargs) as broker:
        for imageDTO in scan_dirs(*paths):
            reply = await broker.send_image(imageDTO, want_reply=want_reply)
            print(f"{imageDTO.name} is {reply}")
            yield reply


async def runall(*args, **kwargs):
    return [i async for i in run(*args, **kwargs)]


@click.command(context_settings={"ignore_unknown_options": True})
@click.argument('paths', nargs=-1)
@click.option('--nats-servers', envvar='NATS_SERVER', multiple=True, default=["127.0.0.1:4222"])
def main(nats_servers: [str] = [], paths: [str] = [], **kwargs):
    if not paths:
        paths = ["."]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(runall(*paths, nats_servers=nats_servers, **kwargs))

if __name__ == "__main__":
    main()
    
