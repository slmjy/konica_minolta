import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import ImageUploader from './component/index.js';
import { rainbow } from 'react-syntax-highlighter/styles/hljs';

export default class App extends React.PureComponent {
    render() {
        return (
            <div className="page">
                <h1>Welcome to the most sophisticated image color analyzer</h1>
                <p>Just put our image here it will go to space and come back with the answer</p>
                <div className="head">Demo</div>
                <ImageUploader 
                               withPreview={true} />
              
            </div>
        );
    }
}
