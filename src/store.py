import asyncio
import click
import os
import sys
import time

import numpy as np
from imagelib import ImageDTO, NATSImageBroker, get_closest_webcolor

import logging
logging.basicConfig(level=logging.DEBUG)

def save_image(image: ImageDTO, save_folder: str = "."):
    target_folder = os.path.join(save_folder, image.color)
    if not os.path.exists(target_folder):
        os.makedirs(target_folder)

    image.save(os.path.join(target_folder, image.name))


async def run(save_folder=".", max_msgs=0,  nats_servers: [str] = [os.environ.get("NATS_SERVER", "127.0.0.1:4222")], **kwargs):

    async with NATSImageBroker(servers=nats_servers, **kwargs) as broker:
        queue, _ = await broker.subscribe(subject="analyzed", max_msgs=max_msgs)

        while True:
            new_image = await queue.get()
            new_image.color = get_closest_webcolor(new_image.color)
            save_image(new_image, save_folder)

            await broker.reply_to_image(new_image)


@click.command(context_settings={"ignore_unknown_options": True})
@click.argument('target', default=".", type=click.Path(exists=True))
@click.option('--nats-servers', envvar='NATS_SERVER', multiple=True, default=["127.0.0.1:4222"])
def main(target: str = ".", nats_servers: [str] = [], **kwargs):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(save_folder=target, nats_servers=nats_servers, **kwargs))

if __name__ == "__main__":
    main()