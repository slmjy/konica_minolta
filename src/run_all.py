import asyncio
import functools
import os
import threading
from concurrent.futures import ThreadPoolExecutor

import pytest

from analyze import run as run_analyze
from collect import run as run_collect
from store import run as run_store


async def run(resources_dir="tests/resources"):

    loop = asyncio.get_event_loop()
    loop_worker = asyncio.new_event_loop()

    def worker(future):
        asyncio.set_event_loop(loop_worker)
        asyncio.run(future)

    tp = ThreadPoolExecutor(max_workers=100)
    tp.submit(asyncio.run, run_analyze())
    tp.submit(asyncio.run, run_store())

    await asyncio.sleep(2)

    test_image_name = "china_flag.png"
    test_image_path = os.path.join(resources_dir, test_image_name)
    test_image_color = "Red"
    test_image_result = os.path.join(test_image_color, test_image_name)

    if os.path.exists(test_image_result):
        os.remove(test_image_result)

    async for result in run_collect(test_image_path, want_reply=False):
        await asyncio.sleep(2)
        with open(test_image_result) as f:
            assert f is not None

    async for result in run_collect(test_image_path, want_reply=True):
        await asyncio.sleep(2)
        assert result == test_image_color


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())