import asyncio
import os
import pytest
from imagelib import NATSImageBroker, ImageDTO


@pytest.fixture
def setup_nats():
    server = os.environ.get("NATS_SERVER", "127.0.0.1:4222")
    loop = asyncio.get_event_loop()
    broker: NATSImageBroker = NATSImageBroker(servers=[server], loop=loop)
    dto: ImageDTO = ImageDTO(name="Lakatos", data="Nebudu to delaaaat")

    return server, loop, broker, dto

@pytest.mark.functional
@pytest.mark.asyncio
async def test_NATSImageBroker__can_send__and__recieve_image(setup_nats, resources_dir):

    server, loop, broker, image = setup_nats
    image = ImageDTO.from_file(f"{resources_dir}/china_flag.png")

    async with broker as _:
        queue, _ = await broker.subscribe()
        await broker.send_image(image)

        result = await queue.get()

        assert result.data == image.data
        assert result.name == image.name

