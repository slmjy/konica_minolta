import asyncio
import pytest
import os
from analyze import run as run_analyze
from collect import runall as run_collect
from store import run as run_store


@pytest.fixture
def test_image(tmpdir, resources_dir):
    test_image_name = "china_flag.png"
    test_image_path = os.path.join(resources_dir, test_image_name)
    test_image_color = "orange red"
    test_image_result = os.path.join(tmpdir, test_image_color, test_image_name)

    if os.path.exists(test_image_result):
        os.remove(test_image_result)

    yield test_image_name, test_image_path, test_image_result, test_image_color



@pytest.mark.functional
@pytest.mark.asyncio
async def test_collect_analy_store__image_will_endup_in_local_folder(test_image, tmpdir):

    test_image_name, test_image_path, test_image_result, test_image_color = test_image

    loop = asyncio.get_event_loop()
    store_task = asyncio.run_coroutine_threadsafe(run_store(tmpdir, max_msgs=2), loop)
    analyze_task = asyncio.run_coroutine_threadsafe(run_analyze(max_msgs=2), loop)

    await asyncio.sleep(1)
    await run_collect(test_image_path, want_reply=False)
    await asyncio.sleep(1)

    assert os.path.exists(test_image_result) == True

    loop.set_exception_handler(lambda loop, context: None)
    for f in [store_task, analyze_task]:
        print(f"Cancelling {f} {f.cancel()}")

    await asyncio.sleep(2) # to avoid millions if task errors. May be it can be done better, but I've spent several hours on this already :(


@pytest.mark.functional
@pytest.mark.asyncio
async def test_collect_analy_store__collect_can_recieve_reply(test_image, tmpdir):

    test_image_name, test_image_path, test_image_result, test_image_color = test_image

    loop = asyncio.get_event_loop()
    store_task = asyncio.run_coroutine_threadsafe(run_store(tmpdir, max_msgs=2), loop)
    analyze_task = asyncio.run_coroutine_threadsafe(run_analyze(max_msgs=2), loop)

    await asyncio.sleep(1)

    assert await run_collect(test_image_path, want_reply=True) == [test_image_color]
    assert os.path.exists(test_image_result) == True

    loop.set_exception_handler(lambda loop, context: None)
    for f in [store_task, analyze_task]:
        print(f"Cancelling {f} {f.cancel()}")

    await asyncio.sleep(2) # to avoid millions if task errors. May be it can be done better, but I've spent several hours on this already :(
