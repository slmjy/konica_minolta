from imagelib import ImageDTO

class InlineClass:
    def __init__(self, **kwargs):
        self.__dict__ = kwargs


def test_imageDTO_can_convert_png_to_nats_and_back(resources_dir):

    image = ImageDTO.from_file(f"{resources_dir}/china_flag.png")
    image_converted = ImageDTO.from_nats_message(InlineClass(data = image.to_nats_msg(), reply = None))
    
    assert image.name == image_converted.name
    assert image.data == image_converted.data
    assert image.from_nats_message == image_converted.from_nats_message
