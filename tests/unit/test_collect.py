import os
from collect import scan_dirs


def test__scan_dir_finds_batman_in_resources(resources_dir):
    files = [ s.name for s in scan_dirs(os.path.join(resources_dir, "../"))]

    assert 'batman.jpg' in files

    
