import numpy as np
from imagelib import ImageDTO, get_image_color_avg, get_closest_webcolor, is_file_image


def create_blank_image_array(width, height, rgb_color=(0, 0, 0)):
    """Create new image(numpy array) filled with certain color in RGB"""

    image = np.zeros((height, width, 3), np.uint8)
    color = tuple(reversed(rgb_color))
    image[:] = color

    return image


def test_get_image_color_avg(resources_dir):

    create_blank_image_array(100, 100, [160, 16, 10])

    assert (
        ImageDTO.from_file(f"{resources_dir}/china_flag.png")
        .calculate_average_color()
        .lower()
        == "#de2d0f"
    ), "China flag is very red"
    assert (
        get_image_color_avg(create_blank_image_array(100, 100, [160, 16, 10])).lower()
        == "#a0100a"
    ), "Values should be reversed"


def test_get_closest_webcolor():
    assert get_closest_webcolor("#FFFFFF") == "white"
    assert get_closest_webcolor("#EEEEEE") == "white smoke"
    assert get_closest_webcolor("#FF0000") == "red"
    assert get_closest_webcolor("#de2d0f") == "orange red"
    assert get_closest_webcolor("#b95273") == "indian red"
    assert get_closest_webcolor("#000000") == "black"



def test_is_file_image(resources_dir):

    assert (
        is_file_image(f"{resources_dir}/emtpty_image.jpg") == False
    ), "empty file is not a valid image to be analyzed"
    assert (
        is_file_image(f"{resources_dir}/Monty-Python.jpg") == True
    ), "This should be a valid image"
    assert (
        is_file_image(f"{resources_dir}/Monty-Python.NotAnImageExtention") == True
    ), "Extention should not matter if it's an image"
    assert (
        is_file_image(f"{resources_dir}/not_image.jpg") == False
    ), "Even if extention is jpg if content is an image it is an image"
