# Welcome to the magnificent Image Collector so to speak


## Architectual decisions and remarks
* **Broker - NATS**: At first I wanted to go with Rabbit and celery, but then I got curious about NATS, as I've never used it or heard about it and decided to go with NATS. Huge mistake - I wasted a lot of time. And now I feel that NATS is not even the best solution here. At the very least I should've used NATs Streaming. I also had problems with replying to the original message from the second subscriber in the chain, bypassing the first subscriber.
* **No cluster for NATS** - I wanted to deploy nats operator to kubernetes and support nats cluster in code, because it makes sense to have persistent things in k8s governed by operators. But in the end the task already took too much time.
* **pydentic** over **dataclasses** because of pickle integration.
* **NATS is not mocked in tests** because functional end-to-end tests are always more useful then unit and with docker-compose it's very easy to have an actual NATS in place.
* **Image related logic is in one package** - although it goes agains the idea of microservices being autonomous it made sense here, as it appears that that it would've changed together in real-life scenarios. After all it can be easily duplicated/refactored to be separate.
* **Flask for web backend and React for front-end**


## Resulting artifacts of the project are:
* Docker images, also on docker hub
	* [slmjy/konica_test_aershov_collect](https://hub.docker.com/repository/docker/slmjy/konica_test_aershov_collect)
	* [slmjy/konica_test_aershov_analyze](https://hub.docker.com/repository/docker/slmjy/konica_test_aershov_analyze)
	* [slmjy/konica_test_aershov_store](https://hub.docker.com/repository/docker/slmjy/konica_test_aershov_store)
	* [slmjy/konica_test_aershov_web](https://hub.docker.com/repository/docker/slmjy/konica_test_aershov_web)
* Helm Chart in [k8s](./k8s) folder to install the whole system to Kubernetes cluster
* [docker-compose.yml](./docker-compose.yml) file to build, test and publish

## How to build and publish images
`sh build.sh` _will run tests also. Contains environment variables for registry to push to_

## How to test
* In root folder run
    * `docker-compose build test && docker-compose run test` 
    * _You will need to rebuild test container only if requirements changed, otherwise the new code will be used because it is mounted as a volume_
* Alternatively (no docker-compose):
    * setup NATS to run on 127.0.0.1:4222 or overrride this by setting NATS_ADRESS environment variable _Or just run docker-compose run nats_
    * Run 
```
pip3 install -r requirements-dev.txt
py.test -vv
```
## How to Kubernetes
* Run charts
    * Install helm**3** *did not test it with helm2 and tiller*
    * have kubernetes cluster configured (I've used Docker Desktop, I hope it will work with minicube )
    * run `helm --namespace konica-aershov install --create-namespace konica-aershov ./k8s`
    * helm will crate a NodePort service, you will need to find the port. Helm will try to help with NOTES at the end

## How to run web interface locally

`docker-compose build`
`docker-compose up -d web` <- you can access it on `http://localhost:3124`

The interface should look like this:

![Screenshot](/doc/web_screenshot.png)