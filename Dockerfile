# ----------------------------------------------------
#       Base container for python services
# ----------------------------------------------------
FROM python:3.8-slim as base

# opencv-python did not work without smth like that
RUN apt-get update ##[edited]
RUN apt-get install 'ffmpeg' \
    'libsm6' \ 
    'libxext6' -y && \
    apt-get clean

RUN mkdir /app
WORKDIR /app
COPY ./requirements.txt /app/requirements.txt
RUN pip3 install -r requirements.txt

# ----------------------------------------------------
#       Build container for node.js
# ----------------------------------------------------
FROM node as node_builder

RUN mkdir /app
WORKDIR /app
COPY ./src/web/package.json /app/package.json
RUN npm install

COPY ./src/web /app/
RUN ls -l /app
RUN npm run build

# ----------------------------------------------------
#       Test container
# ----------------------------------------------------
FROM base as test
#COPY src/requirements.txt /app/requirements.txt
#COPY src/requirements-dev.txt /app/requirements-dev.txt
#RUN pip3 install -r requirements-dev.txt

#COPY src /app/src
#COPY tests /app/tests

# :((
# I would love to copy file in a granular way to reduce image size but Windows docker just fails when it's executed as docker-compose build. 

COPY . /app/
RUN pip3 install -r requirements-dev.txt
CMD py.test


FROM base as prod_base
COPY ./src /app/
RUN ls -l /app

FROM prod_base as collect
ENTRYPOINT ["python3", "/app/collect.py"]
CMD []

FROM prod_base as analyze
ENTRYPOINT python3 /app/analyze.py

FROM prod_base as store
ENTRYPOINT ["python3", "/app/store.py"]
CMD []

FROM prod_base as web
COPY --from=node_builder /app/build/ /app/web/build/
RUN ls -l /app/web
RUN ls -l /app/web/build
EXPOSE 80
ENTRYPOINT ["python3", "/app/web.py"]
CMD ["--port", "80","--host","0.0.0.0"]